export default {
  noBrewTips:
    'It has been detected that you do not have Brew installed, do you want to install it now?',
  brewInstallFail: 'Install Brew Fail',
  userNoInstall: 'User cancels installation',
  clean: 'Clean',
  ok: 'OK',
  versionNoFound: 'Failed to get software version, operation failed.',
  setup: 'Setup',
  savePath: 'Save Dir',
  saveAs: 'Site Save: Save Dir/Site Host',
  proxy: 'Proxy',
  pageLimit: 'Page Limit',
  pageLimitTips: 'Limit page address must contain the string here',
  LinkExclusion: 'Link Exclusion',
  LinkExclusionTips: 'Filter URLs containing this string, one per line.'
}
