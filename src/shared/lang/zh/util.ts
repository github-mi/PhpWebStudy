export default {
  noBrewTips: '检测到您未安装Brew, 是否现在安装?',
  brewInstallFail: 'Brew安装失败',
  userNoInstall: '用户未选择安装',
  clean: '清理',
  ok: '确定',
  versionNoFound: '未获取到软件版本,操作失败',
  setup: '设置',
  savePath: '保存路径',
  saveAs: '文件保存为: 保存路径/域名',
  proxy: '网络代理',
  pageLimit: '页面限制',
  pageLimitTips: '限制页面地址必须包含此处字符串',
  LinkExclusion: '链接排除',
  LinkExclusionTips: '过滤包含此处字符串的网址, 每行一个'
}
